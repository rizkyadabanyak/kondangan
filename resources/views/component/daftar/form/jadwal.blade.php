@extends('landing')

@section('styles')
@endsection

@section('content')
    <style>
        .form-control{
            border: 1px solid #2c89e7;
        }
    </style>

    <div class="">
        <div class="container ">
            <br><br>
            @include('component.daftar.app')
            <br>

            <form method="post" action="{{route('nikah.dateSubmit')}}">
                @csrf
                <div class="row mx-0">
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-bold SourceSansPro biru-tua">Pengajuan Tanggal Nikah</legend>
                        <div class="row">
                            <div class="col-sm-6 biru">
                                <div class="form-group">
                                    <label class="font-weight-bold">Hari <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="day" value="{{($data != null) ? $data->day : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                 </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tanggal Pernikahan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input class="form-control" type="date" value="{{($data != null) ? \Carbon\Carbon::parse($data->date)->format('Y-m-d') : '' }}" name="date" id="example-date-input">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Jam Pernikahan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input class="form-control" type="time" name="time" value="{{($data != null) ? $data->time : '' }}" id="example-time-input">
                                </div>
                            </div>
                            <div class="col-sm-6 biru">
                                <div class="form-group">
                                    <label class="font-weight-bold">Mas Kawin <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="dowry" value="{{($data != null) ? $data->dowry : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Utang ?  <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <!-- Default switch -->
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" name="debt" class="custom-control-input" id="customSwitches" {{($data != null && $data->debt == 1) ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="customSwitches"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" value="{{($data != null) ? $data->place : '' }}" name="place" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Temat...">
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>
            <div class="text-center mt-3">
                <button type="submit" href="" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Yey Nikah</button>
            </div>
            </form>
            <br>
        </div>
    </div>
@endsection
