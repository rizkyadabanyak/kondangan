@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Lengkapi Formulir Pengajuan Nikah</h1>
            <br><br>
            <div class="row" style="height: 500px">
                <div class="col-md-6 align-self-center text-center">
                    <a href="{{route('nikah.cowo.S1Biodatacowo')}}"><img src="{{asset('img/caloncowo.png')}}" width="250" class="img-fluid" alt="" style="padding-bottom: 50px"></a>
                </div>
                <div class="col-md-6 SourceSansPro align-self-center text-center ">
                    <a href="{{route('nikah.cewe.S1Biodatacewe')}}"><img src="{{asset('img/caloncewe.png')}}" width="250" class="img-fluid" alt="" style="padding-bottom: 50px">
                    </a>
                </div>
            </div>
            <div class="text-center mt-3">
                @if($data->biodata_male_id ==null || $data->biodata_female_id ==null)
                    <a href="{{route('nikah.date')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                @else
                    <div class="row">
                        <div class="col-md-6 ">
                            <a href="{{route('nikah.date')}}" class="btn btn-primary float-right" style="width: 200px;background-color: #2a92d7">Back</a>
                        </div>
                        <div class="col-md-6">
                            <form action="{{route('nikah.verifikasiSubmit')}}" class="float-left" method="post">
                                @csrf
                                <input type="text" value="1" name="final" hidden>
                                <a class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Finish</a>
                            </form>
                        </div>
                    </div>

                @endif

            </div>
            <br><br>
        </div>

    </div>
@endsection
