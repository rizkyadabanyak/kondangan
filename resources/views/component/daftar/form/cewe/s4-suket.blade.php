@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Surat Keterangan Tentang Orang Tua </h1>
            <br><br><br><br>
            @include('component.daftar.form.step')
            <br><br>
            <div class="row mx-0">
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-bold SourceSansPro biru-tua">Bio Data Orang Tua</legend>
                        <div class="row">
                            <div class="col-sm-6 biru">
                                <h1 class="text-center biru SourceSansPro font-weight-bold mt-3 mb-3">AYAH</h1>

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_nameayah" value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="nameayah" value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birthayah" value="{{($ibu->FemaleCandidate->perenFathertFemale != null) ? $ibu->FemaleCandidate->perenFathertFemale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($ibu->FemaleCandidate->perenFathertFemale != null) ? \Carbon\Carbon::parse($ibu->FemaleCandidate->perenFathertFemale->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birthayah" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizenayah" value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religionayah" class="form-control">
                                        <option value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->religion : '' }}" selected>{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->religion : '' }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="professionayah" value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residenceayah" value="{{($ayah->FemaleCandidate->perenFathertFemale != null) ? $ayah->FemaleCandidate->perenFathertFemale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>


                            <div class="col-sm-6 biru">
                                <h1 class="text-center biru SourceSansPro font-weight-bold mt-3 mb-3">IBU</h1>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_nameibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="nameibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birthibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? \Carbon\Carbon::parse($ibu->FemaleCandidate->perenMothertFemale->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birthibu" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizenibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religionibu" class="form-control">
                                        <option value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->religion : '' }}" selected>{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->religion : '' }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="professionibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residenceibu" value="{{($ibu->FemaleCandidate->perenMothertFemale != null) ? $ibu->FemaleCandidate->perenMothertFemale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>

            <div>
                <div class="row">

                    <div class="col-md-12">
                        <fieldset>
                            <legend class="font-weight-bold SourceSansPro biru-tua">Ayah Kandung & Ibu Kandung Dari Seorang</legend>

                            <div class="row">
                                <div class="col-sm-6 biru">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="full_name" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="name" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <input type="text" name="place_of_birth" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control" type="date" value="{{($data->FemaleCandidate != null) ? \Carbon\Carbon::parse($data->FemaleCandidate->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birth" id="example-date-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="citizen" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>


                                </div>
                                <div class="col-sm-6 biru">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <select id="inputState" class="form-control" name="religion">
                                            <option value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->religion : '' }}" selected>{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->religion : '' }}</option>
                                            <option value="islam">Islam</option>
                                            <option value="hindu">hindu</option>
                                            <option value="budha">budha</option>
                                            <option value="krtisten">krtisten</option>
                                            <option value="protestan">protestan</option>
                                            <option value="konghuchu">konghuchu</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="profession" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="residence" value="{{($data->FemaleCandidate != null) ? $data->FemaleCandidate->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="text-center mt-3">
                <a href="" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                <a href="{{route('nikah.cewe.S5sujincewe')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Next</a>
            </div>
            <br><br>
        </div>
    </div>
@endsection
