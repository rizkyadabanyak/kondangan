@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <form method="post" action="{{route('nikah.cewe.S6sukematceweSubmit')}}">
                @csrf
            <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Surat Keterangan Kematian</h1>
            <br><br><br><br>
            @include('component.daftar.form.step')
            <br><br>
            <div class="row mx-0">
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-bold SourceSansPro biru-tua">Biodata</legend>
                        <div class="row">
                            <div class="col-sm-12 biru">

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_name" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="name" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birth" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($data->FemaleCandidate->extFemale != null) ? \Carbon\Carbon::parse($data->FemaleCandidate->extFemale->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birth" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizen" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religion" class="form-control">
                                        <option value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->religion : '' }}" selected>{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->religion : '' }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="profession" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residence" value="{{($data->FemaleCandidate->extFemale != null) ? $data->FemaleCandidate->extFemale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>
            <div class="text-center mt-3">
                <a href="{{route('nikah.cewe.S5sujincewe')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                <button class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Finish</button>
            </div>
            <br><br>
            </form>
        </div>
    </div>
@endsection
