@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <form method="post" action="{{route('nikah.cowo.S6sukematcowoSubmit')}}">
                @csrf
            <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Surat Keterangan Kematian</h1>
            <br><br><br><br>
            @include('component.daftar.form.step')
            <br><br>
            <div class="row mx-0">
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-bold SourceSansPro biru-tua">Biodata</legend>
                        <div class="row">
                            <div class="col-sm-12 biru">

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_name" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="name" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birth" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($data->MaleCandidate->extMale != null) ? \Carbon\Carbon::parse($data->MaleCandidate->extMale->date_of_birth)->format('Y-m-d') : '00-00-0000' }}" name="date_of_birth" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizen" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religion" class="form-control">
                                        <option value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->religion : ' ' }}" selected>{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->religion : null }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="profession" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residence" value="{{($data->MaleCandidate->extMale != null) ? $data->MaleCandidate->extMale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>
            <div class="text-center mt-3">
                <a href="{{route('nikah.cowo.S5sujincowo')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                <button class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Finish</button>
            </div>
            <br><br>
            </form>
        </div>
    </div>
@endsection
