@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <form action="{{route('nikah.cowo.S1BiodatacowoSubmit')}}" method="post">
                @csrf
                <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Selamat Datang </h1>
            <br><br><br><br>
            @include('component.daftar.form.step')
            <br><br>
            <div>
                <div class="row">
                    <div class="col-md-3">
                        <fieldset>
                            <legend class="font-weight-bold SourceSansPro biru-tua">Suket Nikah</legend>
                            <div class="form-group biru">
                                <label class="font-weight-bold">Nomor <a style="font-size: 20px" class="text-danger">*</a></label>
                                <input type="text" class="form-control" name="letter" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->letter : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nomor...">
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend class="font-weight-bold SourceSansPro biru-tua">Kantor</legend>
                            <div class="form-group biru">
                                <label class="font-weight-bold">Kantor /Desa /Kelurahan <a style="font-size: 20px" class="text-danger">*</a></label>
                                <input type="text" class="form-control" name="village" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->village : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nomor...">
                            </div>
                            <div class="form-group biru">
                                <label class="font-weight-bold">Kecamatan <a style="font-size: 20px" class="text-danger">*</a></label>
                                <input type="text" class="form-control" name="district" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->district : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nomor...">
                            </div>
                            <div class="form-group biru">
                                <label class="font-weight-bold">Kabupaten/Kota <a style="font-size: 20px" class="text-danger">*</a></label>
                                <input type="text" class="form-control" name="city" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->city : '' }}" aria-describedby="emailHelp" placeholder="Masukkan Nomor...">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-9">
                        <fieldset>
                            <legend class="font-weight-bold SourceSansPro biru-tua">Data Diri Calon Laki - Laki</legend>

                            <div class="row">
                                <div class="col-sm-6 biru">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="full_name" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="name" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <input type="text" name="place_of_birth" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control" type="date" value="{{($data->MaleCandidate != null) ? \Carbon\Carbon::parse($data->MaleCandidate->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birth" id="example-date-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="citizen" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <select id="inputState" class="form-control" name="religion">
                                            <option value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->religion : '' }}" selected>{{($data->MaleCandidate != null) ? $data->MaleCandidate->religion : '' }}</option>
                                            <option value="islam">Islam</option>
                                            <option value="hindu">hindu</option>
                                            <option value="budha">budha</option>
                                            <option value="krtisten">krtisten</option>
                                            <option value="protestan">protestan</option>
                                            <option value="konghuchu">konghuchu</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-sm-6 biru">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="profession" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="residence" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Bin/Binti <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="bin_binti" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->bin_binti : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Status Perkawinan <a style="font-size: 20px" class="text-danger">*</a></label>
                                        <input type="text" class="form-control" name="status" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->status : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-bold">Nama Isti Dulu <a style="font-size: 20px" class="text-danger"></a></label>
                                        <input type="text" class="form-control" name="ex_spouse" value="{{($data->MaleCandidate != null) ? $data->MaleCandidate->ex_spouse : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Mas Kawin...">
                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>


            <div class="text-center mt-3">
                <a href="{{route('nikah.pilih')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                <button href="{{route('nikah.cowo.S1BiodatacowoSubmit')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Next</button>
            </div>
            <br><br>
            </form>
        </div>
    </div>
@endsection
