@extends('landing')

@section('content')
    <div class="">
        <div class="container ">
            <form action="{{route('nikah.cowo.S2AsalusulcowoSubmit')}}" method="post">
            @csrf
            <br><br>
            <h1 class="biru-tua text-center SourceSansPro">Surat Keterangan Asal Usul </h1>
            <br><br><br><br>
            @include('component.daftar.form.step')
            <br><br>
            <div class="row mx-0">
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-bold SourceSansPro biru-tua">Asal Usul Calon Laki-Laki</legend>
                        <div class="row">
                            <div class="col-sm-6 biru">
                                <h1 class="text-center biru SourceSansPro font-weight-bold mt-3 mb-3">AYAH</h1>

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_nameayah" value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="nameayah" value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birthayah" value="{{($ibu->MaleCandidate->perenFathertMale != null) ? $ibu->MaleCandidate->perenFathertMale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($ibu->MaleCandidate->perenFathertMale != null) ? \Carbon\Carbon::parse($ibu->MaleCandidate->perenFathertMale->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birthayah" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizenayah" value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religionayah" class="form-control">
                                        <option value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->religion : '' }}" selected>{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->religion : '' }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="professionayah" value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residenceayah" value="{{($ayah->MaleCandidate->perenFathertMale != null) ? $ayah->MaleCandidate->perenFathertMale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>
                            <div class="col-sm-6 biru">
                                <h1 class="text-center biru SourceSansPro font-weight-bold mt-3 mb-3">IBU</h1>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="full_nameibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->full_name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Alias <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="nameibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->name : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" name="place_of_birthibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->place_of_birth : '' }}" class="form-control" placeholder="Tempat">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? \Carbon\Carbon::parse($ibu->MaleCandidate->perenMothertMale->date_of_birth)->format('Y-m-d') : '' }}" name="date_of_birthibu" id="example-date-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Warga Negara <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="citizenibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->citizen : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Agama <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <select id="inputState" name="religionibu" class="form-control">
                                        <option value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->religion : '' }}" selected>{{($ibu->MaleCandidate->perenFathertMale != null) ? $ibu->MaleCandidate->perenMothertMale->religion : '' }}</option>
                                        <option value="islam">islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="budha">budha</option>
                                        <option value="krtisten">krtisten</option>
                                        <option value="protestan">protestan</option>
                                        <option value="konghuchu">konghuchu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Pekerjaan <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="professionibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->profession : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tinggal <a style="font-size: 20px" class="text-danger">*</a></label>
                                    <input type="text" class="form-control" name="residenceibu" value="{{($ibu->MaleCandidate->perenMothertMale != null) ? $ibu->MaleCandidate->perenMothertMale->residence : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Hari...">
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>
            <div class="text-center mt-3">
                <a href="{{route('nikah.cowo.S1BiodatacowoSubmit')}}" class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Back</a>
                <button class="btn btn-primary" style="width: 200px;background-color: #2a92d7">Next</button>
            </div>
            <br><br>
            </form>
        </div>
    </div>
@endsection
