@extends('landing')

@section('content')
<div class="">
    <div class="container ">
        <div class="row" style="height: 550px">

            <div class="col-12 col-md-7 align-self-center text-center">
                <img src="{{asset('img/nikah.png')}}" width="500" class="img-fluid" alt="">
            </div>
            <div class="col-md-5 SourceSansPro align-self-center ">
                <h1 style="color: #838383" class="">
                    Yuk Ajukan Nikah Dengan Lebih Mudah Dengan WEB kondangan
                </h1>
                <br>
                <a href="{{route('nikah.redirect')}}" type="button" class="btn bg-orange text-white pd-20">DAFTAR</a>
            </div>
        </div>
    </div>
</div>
@endsection
