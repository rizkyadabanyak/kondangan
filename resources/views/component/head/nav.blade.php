<nav class="navbar navbar-expand-lg navbar-light bg-biru-tua sticky-top " style="top: -1px">
    <div class="d-xl-none d-lg-none text-center d-flex justify-content-center" style="width: calc(100% - 56px);">
        <a class="navbar-bran " href="{{url('/')}}">
            <img src="{{asset('img/logo.png')}}" width="200" class="d-inline-block align-top " alt="">
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto align-self-center" style="padding-bottom: 10px;padding-top: 10px">
            <li class="nav-item active pd-50">
                <a class="nav-link text-white font-weight-bold" href="{{url('/')}}">BERANDA <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active pd-50 font-weight-bold">
                <a class="nav-link text-white" href="">ARTIKEL <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active pd-50 font-weight-bold" >
                <a class="nav-link text-white" href="{{route('pengajuan')}}">DAFTAR CALON <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <div class="pdr-50" >
            @guest
                @if (Route::has('login'))
                    <a href="{{ route('login') }}"><button type="button" class="btn btn-outline-light" style="margin-right: 30px">MASUK</button></a>

                @endif

                @if (Route::has('register'))
                        <a href="{{ route('register') }}"><button type="button" class="btn btn-light">DAFTAR</button></a>

                    @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest

        </div>
        <br>
    </div>
</nav>
