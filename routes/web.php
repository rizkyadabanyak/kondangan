<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('component/content');
})->name('landing');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/pengajuan', [App\Http\Controllers\NikahController::class, 'pengajuan'])->name('pengajuan');

Route::namespace('App\Http\Controllers')->name('nikah.')->middleware(['web'])->prefix('nikah')->group(function () {
    Route::get('/tanggal', [App\Http\Controllers\NikahController::class, 'Date'])->name('date');
    Route::post('/tanggal', [App\Http\Controllers\NikahController::class, 'DateSumbit'])->name('dateSubmit');
    Route::get('/daftar', [App\Http\Controllers\NikahController::class, 'redirect'])->name('redirect');

    Route::get('/pilih', [App\Http\Controllers\NikahController::class, 'Pilih'])->name('pilih');

    Route::post('/pilih', [App\Http\Controllers\NikahController::class, 'verifikasisubmit'])->name('verifikasiSubmit');


    Route::name('cowo.')->prefix('cowo')->group(function () {
        Route::get('/s1-biodata', [App\Http\Controllers\NikahController::class, 'S1Biodatacowo'])->name('S1Biodatacowo');
        Route::post('/s1-biodata', [App\Http\Controllers\NikahController::class, 'S1BiodatacowoSubmit'])->name('S1BiodatacowoSubmit');
        Route::get('/s2-asalusul', [App\Http\Controllers\NikahController::class, 'S2Asalusulcowo'])->name('S2Asalusulcowo');
        Route::post('/s2-asalusul', [App\Http\Controllers\NikahController::class, 'S2AsalusulcowoSubmit'])->name('S2AsalusulcowoSubmit');
        Route::get('/s3-persetujuan', [App\Http\Controllers\NikahController::class, 'S3PersetujuanMempelaicowo'])->name('S3PersetujuanMempelaicowo');
        Route::post('/s3-persetujuan', [App\Http\Controllers\NikahController::class, 'S3PersetujuanMempelaicowoSubmit'])->name('S3PersetujuanMempelaicowoSubmit');
        Route::get('/s4-suketortu', [App\Http\Controllers\NikahController::class, 'S4suketortucowo'])->name('S4suketortucowo');
        Route::post('/s4-suketortu', [App\Http\Controllers\NikahController::class, 'S4suketortucowoSubmit'])->name('S4suketortucowoSubmit');
        Route::get('/s5-sujin', [App\Http\Controllers\NikahController::class, 'S5sujincowo'])->name('S5sujincowo');
        Route::post('/s5-sujin', [App\Http\Controllers\NikahController::class, 'S5sujincowoSubmit'])->name('S5sujincowoSubmit');
        Route::get('/s6-sukemat', [App\Http\Controllers\NikahController::class, 'S6sukematcowo'])->name('S6sukematcowo');
        Route::post('/s6-sukemat', [App\Http\Controllers\NikahController::class, 'S6sukematcowoSubmit'])->name('S6sukematcowoSubmit');
    });

    Route::name('cewe.')->prefix('cewe')->group(function () {
        Route::get('/s1-biodata', [App\Http\Controllers\NikahController::class, 'S1Biodatacewe'])->name('S1Biodatacewe');
        Route::post('/s1-biodata', [App\Http\Controllers\NikahController::class, 'S1BiodataceweSubmit'])->name('S1BiodataceweSubmit');
        Route::get('/s2-asalusul', [App\Http\Controllers\NikahController::class, 'S2Asalusulcewe'])->name('S2Asalusulcewe');
        Route::post('/s2-asalusul', [App\Http\Controllers\NikahController::class, 'S2AsalusulceweSubmit'])->name('S2AsalusulceweSubmit');
        Route::get('/s3-persetujuan', [App\Http\Controllers\NikahController::class, 'S3PersetujuanMempelaicewe'])->name('S3PersetujuanMempelaicewe');
        Route::post('/s3-persetujuan', [App\Http\Controllers\NikahController::class, 'S3PersetujuanMempelaiceweSubmit'])->name('S3PersetujuanMempelaiceweSubmit');
        Route::get('/s4-suketortu', [App\Http\Controllers\NikahController::class, 'S4suketortucewe'])->name('S4suketortucewe');
        Route::post('/s4-suketortu', [App\Http\Controllers\NikahController::class, 'S4suketortuceweSubmit'])->name('S4suketortuceweSubmit');
        Route::get('/s5-sujin', [App\Http\Controllers\NikahController::class, 'S5sujincewe'])->name('S5sujincewe');
        Route::post('/s5-sujin', [App\Http\Controllers\NikahController::class, 'S5sujinceweSubmit'])->name('S5sujinceweSubmit');
        Route::get('/s6-sukemat', [App\Http\Controllers\NikahController::class, 'S6sukematcewe'])->name('S6sukematcewe');
        Route::post('/s6-sukemat', [App\Http\Controllers\NikahController::class, 'S6sukematceweSubmit'])->name('S6sukematceweSubmit');
    });
});

